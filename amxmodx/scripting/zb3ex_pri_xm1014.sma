/* Plugin generated by AMXX-Studio */

#include <amxmodx>
#include <fakemeta_util>
#include <cstrike>
#include <zombie_thehero2>

#define PLUGIN "[ZB3EX] XM1014"
#define VERSION "2.0"
#define AUTHOR "Dias"

new g_xm1014

public plugin_init() 
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
}

public plugin_precache()
{
	g_xm1014 = zb3_register_weapon("Benelli M4 (XM1014)", WPN_PRIMARY, 0)
}

public zb3_weapon_selected_post(id, wpnid)
{
	if(wpnid == g_xm1014) get_xm1014(id)
}

public get_xm1014(id)
{
	fm_give_item(id, "weapon_xm1014")
	cs_set_user_bpammo(id, CSW_XM1014, 100)
}
